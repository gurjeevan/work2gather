jQuery(document).ready(function(){
    
    jQuery(document).on('click','.deleteData',function(){
        var deleteId = jQuery(this).attr('data-id');
        var tableName = jQuery(this).attr('data-target');
        bootbox.confirm("Do you really want to delete this record?", function(result) {
            if(result){
                jQuery.ajax({
                    type: "Get",
                    url: site_url+'/admin/delete',
                    data: {"deleteId":deleteId,"tableName":tableName},
                    dataType: "json",
                    success: function(response){
                        if(response == 1)
                        {
                            jQuery("#row_"+deleteId).remove();
                            setTimeout(function(){ location.reload(); }, 2000);
                        }
                    }
                });
            }
        });
   });
   
   jQuery(document).on('click','.updateStatus',function(){
        var updateId = jQuery(this).attr('data-id');
        var tableName = jQuery(this).attr('data-target');
        var status = jQuery(this).attr('data-status');
        if(status == 0)
        {
            var vr = "Do you really want to deactivate this record?";
        }
        else
        {
            var vr = "Do you really want to activate this record?";
        }
        bootbox.confirm(vr, function(result) {
            if(result){
                jQuery.ajax({
                    type: "Get",
                    url: site_url+'/admin/update_status',
                    data: {"status":status,"updateId":updateId,"tableName":tableName},
                    dataType: "json",
                    success: function(response){
                        if(response == 1)
                        {
                            location.reload();
                        }
                    }
                });
            }
        });
   });
   
   jQuery(document).on('click','.manage_city',function(){
       var val = jQuery(this).attr('data-attr');
       if(val == 'add')
       {
           jQuery('#cityTitle').html('Add City');
           jQuery('.btn_title').html('Add');
       }
       else
       {
           jQuery('#cityTitle').html('Update City');
           jQuery('.btn_title').html('Update');
       }
       jQuery('#cityModal').modal('show');
   });
   
    function readURL(input) 
    {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) 
        {
            jQuery('#avatarFile').attr('name','profile_image');
            jQuery('#blah').css('display','block');
            jQuery('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    jQuery("#avatarFile").change(function() {
      readURL(this);
    });
    
    // var imagesPreview = function(input, placeToInsertImagePreview) {

    //     if (input.files) {
    //         var filesAmount = input.files.length;

    //         for (i = 0; i < filesAmount; i++) {
    //             var reader = new FileReader();

    //             reader.onload = function(event) {
    //                 jQuery($.parseHTML('<img width="100" height="100">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
    //             }

    //             reader.readAsDataURL(input.files[i]);
    //         }
    //     }

    // };

    // jQuery('#gallery-photo-add').on('change', function() {
    //     imagesPreview(this, 'div.gallery');
    // });
    
    jQuery(".chzn-select").chosen();
    
    
    
});