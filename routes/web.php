<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/*===============Admin routing============*/

Route::get('/admin', 'AdminController@index')->name('admin');

Route::get('/logout', 'AdminController@logout')->name('logout');

Route::get('/admin/add_users', 'AdminController@addUsers')->name('add_users');

Route::post('/admin/add_users', 'AdminController@addUsers')->name('add_users');

Route::get('/admin/add_users/{id}', 'AdminController@addUsers');

Route::get('/admin/manage_users', 'AdminController@manageUsers')->name('manage_users');

Route::get('/admin/delete', 'AdminController@deleteData');

Route::get('/admin/about_us', 'AdminController@aboutUs');

Route::get('/admin/privacy_policy', 'AdminController@privacyPolicy');

Route::get('/admin/terms_condition', 'AdminController@termsCondition');

Route::post('/admin/update_pages', 'AdminController@updatePages');

Route::any('/admin/manage_cities', 'AdminController@manageCities');

Route::get('/admin/add_city', 'AdminController@addCity');

Route::post('/admin/add_city', 'AdminController@addCity');

Route::get('/admin/edit_city/{id}', 'AdminController@editCity');

Route::get('/admin/update_status', 'AdminController@updateStatus');

Route::get('/admin/change_password', 'AdminController@changePassword');

Route::post('/admin/change_password', 'AdminController@changePassword');

Route::get('/admin/manage_profile', 'AdminController@manageProfile');

Route::post('/admin/manage_profile', 'AdminController@manageProfile');

Route::get('/admin/payment_settings', 'AdminController@paymentSettings');

Route::post('/admin/payment_settings', 'AdminController@paymentSettings');

Route::get('/admin/manage_payments', 'AdminController@paymentDetail');

Route::get('/admin/manage_types', 'AdminController@managetype');

Route::get('/admin/manage_durations', 'AdminController@manageDuration');

Route::get('/admin/manage_features', 'AdminController@manageFeatures');

Route::get('/admin/manage_amenities', 'AdminController@manageAmenity');

Route::get('/admin/add_amenity', 'AdminController@addAmenity');

Route::post('/admin/add_amenity', 'AdminController@addAmenity');

Route::get('/admin/add_feature', 'AdminController@addFeature');

Route::post('/admin/add_feature', 'AdminController@addFeature');

Route::get('/admin/add_duration', 'AdminController@addDuration');

Route::post('/admin/add_duration', 'AdminController@addDuration');

Route::get('/admin/add_type', 'AdminController@addType');

Route::post('/admin/add_type', 'AdminController@addType');

Route::get('/admin/edit_amenity/{id}', 'AdminController@editAmenity');

Route::get('/admin/edit_feature/{id}', 'AdminController@editFeature');

Route::get('/admin/edit_duration/{id}', 'AdminController@editDuration');

Route::get('/admin/edit_type/{id}', 'AdminController@editType');

Route::get('/admin/manage_workspace', 'AdminController@manageWorkspace');

Route::get('/admin/add_workspace', 'AdminController@addWorkspace');

Route::post('/admin/add_workspace', 'AdminController@addWorkspace');

Route::get('/admin/faqs', 'AdminController@manageFaqs');

Route::get('/admin/add_faqs', 'AdminController@addFaqs');

Route::post('/admin/add_faqs', 'AdminController@addFaqs');

Route::get('/admin/edit_faqs/{id}', 'AdminController@editFaqs');

Route::get('/admin/contact_us', 'AdminController@ContactUs');

