<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Workspace extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function workspaceType()
    {
        return $this->belongsTo('App\WorkspaceType','workspace_type','id');
    }
     
    
}
