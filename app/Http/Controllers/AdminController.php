<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Auth;
use Hash;
use App\User;
use App\UserMeta;
use App\AdminPage;
use App\ManageCity;
use App\PaymentSetting;
use App\PaymentDetail;
use App\WorkspaceAmenity;
use App\WorkspaceFeature;
use App\WorkspaceType;
use App\WorkspaceDuration;
use App\Workspace;
use App\WorkspaceImage;
use App\WorkspaceVideo;
use App\WorkspaceAmenityMeta;
use App\WorkspaceFeatureMeta;
use App\Faq;
use App\ContactForm;
use Image;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.index');
    }

    public function logout()
    {
        if(Auth::user())
        {
            Auth::logout();
        }
       
        redirect('/admin');
    }

    /*=====================Add users by admin===============*/

    public function addUsers(Request $request, $id = NULL)
    {
        $userData = '';
        if($id != '')
        {
            $userData = User::with('userMeta')->where('id',decrypt($id))->first();
		}
        if($request->all())
        {
            $randomStr = rand();
			if($request->id == '')
			{
				$validator = $request->validate([
					'email' => 'required|unique:users',
					'first_name' => 'required',
					'middle_name' => 'required',
					'last_name' => 'required',
					'password' => 'required|min:6',
					'gender' => 'required',
					'user_type' => 'required',
				]);
				$user = new User();
				$user->name = $request->first_name;
				$user->email = $request->email;
				$user->uniqueId = md5($randomStr);
				$user->password = Hash::make($request->password);
				$user->status = $request->status;
				$user->user_type = $request->user_type;
				$user->email_verification = 'no';
				$user->save();
				
				$user_meta = new UserMeta();
				$user_meta->user_id = $user->id;
				$user_meta->first_name = $request->first_name;
				$user_meta->middle_name = $request->middle_name;
				$user_meta->last_name = $request->last_name;
				$user_meta->gender = $request->gender;
				$user_meta->save();
			}
			else
			{
				$user = User::where('id',$request->id)->first();
				$user->name = $request->first_name;
				$user->email = $request->email;
				$user->password = Hash::make($request->password);
				$user->status = $request->status;
				$user->user_type = $request->user_type;
				$user->save();
				
				$user_meta = UserMeta::where('user_id',$request->id)->first();
				$user_meta->first_name = $request->first_name;
				$user_meta->middle_name = $request->middle_name;
				$user_meta->last_name = $request->last_name;
				$user_meta->gender = $request->gender;
				$user_meta->save();
			}
            return redirect('admin/manage_users');
        }
        return view('admin.add_users',['userData'=>$userData]);
    }

    /*================Show user listing==============*/

    public function manageUsers()
    {
        $users = User::where('user_type','!=','admin')->get();
        return view('admin.manage_users',['users'=>$users]);
    }
    
    /*===========Common Delete Data=========*/
    
	public function deleteData()
	{		
	    $id = $_GET['deleteId'];		
	    $table = $_GET['tableName'];		
	    if($table == 'manage_users')		
	    {			
	        $data = User::find($id);			
	        $data->delete();		
	    }		
	    else if($table == 'manage_cities')		
	    {			
	        $data = ManageCity::find($id);		
	        $data->delete();		
	    }
	    else if($table == 'workspace_amenity')		
	    {			
	        $data = WorkspaceAmenity::find($id);		
	        $data->delete();
	    }
	    else if($table == 'workspace_duration')		
	    {			
	        $data = WorkspaceDuration::find($id);		
	        $data->delete();
	    }
	    else if($table == 'workspace_types')		
	    {			
	        $data = WorkspaceType::find($id);		
	        $data->delete();
	    }
	    else if($table == 'workspace_features')		
	    {			
	        $data = WorkspaceFeature::find($id);		
	        $data->delete();
	    }
	    else if($table == 'manage_faqs')		
	    {			
	        $data = Faq::find($id);		
	        $data->delete();
	    }
	    if($data != '')		
	    {			
	        $deleteData = 1;		
	    }	
	    else		
	    {			
	        $deleteData = 0;		
	    }		
	    echo json_encode($deleteData);exit;	
	}
	
	/*===========Common Update Status==========*/
	
	public function updateStatus()
	{
	    $id = $_GET['updateId'];		
	    $table = $_GET['tableName'];
	    $status = $_GET['status'];
	    if($table == 'manage_cities')		
	    {			
	        $data = ManageCity::find($id);		
	        $data->status = $status;	
	        $data->save();
	    }
	    else if($table == 'manage_users')		
	    {			
	        $data = User::find($id);		
	        $data->status = $status;	
	        $data->save();
	    }
	    else if($table == 'workspace_amenity')		
	    {			
	        $data = WorkspaceAmenity::find($id);		
	        $data->status = $status;	
	        $data->save();
	    }
	    else if($table == 'workspace_duration')		
	    {			
	        $data = WorkspaceDuration::find($id);		
	        $data->status = $status;	
	        $data->save();
	    }
	    else if($table == 'workspace_types')		
	    {			
	        $data = WorkspaceType::find($id);		
	        $data->status = $status;	
	        $data->save();
	    }
	    else if($table == 'workspace_features')		
	    {			
	        $data = WorkspaceFeature::find($id);		
	        $data->status = $status;	
	        $data->save();
	    }
	    else if($table == 'manage_faqs')		
	    {			
	        $data = Faq::find($id);		
	        $data->status = $status;	
	        $data->save();
	    }
	    if($data != '')		
	    {			
	        $update = 1;		
	    }	
	    else		
	    {			
	        $update = 0;		
	    }
	    echo json_encode($update);exit;	
	}
    
    public function aboutUs()
    {
        $data = AdminPage::where('slug','about_us')->first();
        return view('admin.about-us',['data'=>$data]);
    }
    
    public function privacyPolicy()
    {
        $data = AdminPage::where('slug','privacy_policy')->first();
        return view('admin.privacy-policy',['data'=>$data]);
    }
    
    public function termsCondition()
    {		
        $data = AdminPage::where('slug','terms_condition')->first();		
        return view('admin.terms',['data'=>$data]);	
    }
    public function updatePages(Request $request)	
    {		
        $validator = $request->validate([			
            'status' => 'required',			
            'name' => 'required',			
            'textarea_ckeditor' => 'required',		
        ]);		
        if($request->id == '')	
        {			
            $adminPage = new AdminPage();	
        }		
        else		
        {		
            $adminPage = AdminPage::find(decrypt($request->id));	
        }	
        $adminPage->slug = $request->slug;		
        $adminPage->name = $request->name;		
        $adminPage->description = $request->textarea_ckeditor;		
        $adminPage->status = $request->status;		
        $adminPage->save();		
        return redirect('admin/'.$request->slug);	
    }
    
    /*============Manage frontend cities===========*/
    
    public function manageCities()	
    {		
        $cities = ManageCity::get();		
        return view('admin.manage_cities',['cities'=>$cities]);	
    }		
    public function addCity(Request $request)	
    {		
        $city = '';		
        if($request->all())		
        {			
            $validator = $request->validate([				
                'status' => 'required',				
                'city' => 'required',			
            ]);			
            if($request->id == '')			
            {				
                $city = new ManageCity();		
            }			
            else			
            {				
                $city = ManageCity::find(decrypt($request->id));			
            }			
            $city->city = $request->city;			
            $city->status = $request->status;			
            $city->save();			
            return redirect('admin/manage_cities');		
        }		
        return view('admin.add_city',['city'=>$city]);	
    }		
    public function editCity($id)
    {		
        $editId = decrypt($id);		
        $city = ManageCity::find($editId);	
        return view('admin.add_city',['city'=>$city]);	
    }
    
    /*=============Manage Profile==========*/
    
    public function manageProfile(Request $request)
    {
        if($request->all())
        {
            $user = Auth::user();
            if(Auth::user()->email == $request->get('email')) 
            {
                $validatedData = $request->validate([
                    'name' => 'required',
                    'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                if ($request->profile_image) 
                {
                    $file = $request->profile_image;
                    $imageType = $file->getClientmimeType();
                    $fileName = $file->getClientOriginalName();
                    $fileNameUnique = time() . '_' . $fileName;
                    $destinationPath = public_path() . '/assets/uploads/';
                    $file->move($destinationPath, $fileNameUnique);
                    $user->profile_image = $fileNameUnique;
                } 
                $user->name = request('name');
                $user->save();
                return redirect()->back()->with("success","Profile updated successfully !");
            }
            else
            {
                $validatedData = $request->validate([
                    'name' => 'required',
                    'email' => 'required|email|unique:users',
                    'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                $user->name = $request->get('name');
                $user->email = $request->get('email');
                $user->save();
                return redirect()->back()->with("success","Profile updated successfully !");
            }
        }
        return view('admin.manage_profile');
    }
    
    /*=============Change Password=========*/
    
    public function changePassword(Request $request)
    {
        if($request->all())
        {
            if (!(Hash::check($request->get('current_password'), Auth::user()->password))) 
            {
                return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
            }
            if(strcmp($request->get('current_password'), $request->get('new_password')) == 0)
            {
                return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
            }
            $validatedData = $request->validate([
                'current_password' => 'required',
                'new_password' => 'required|string|min:6|confirmed',
            ]);
            $user = Auth::user();
            $user->password = Hash::make($request->get('new_password'));
            $user->save();
            return redirect()->back()->with("success","Password changed successfully !");
        }
        return view('admin.change_password');
    }
    
    /*==============Manage Payments==========*/
    
    public function paymentSettings(Request $request)
    {
        if($request->all())
        {
            $validatedData = $request->validate([
                'api_key' => 'required',
                'secret_key' => 'required',
            ]);
            if($request->id != '')
            {
                $payment = PaymentSetting::find($request->id);
            }
            else
            {
                $payment = new PaymentSetting();
            }
            $payment->api_key = $request->api_key;
            $payment->secret_key = $request->secret_key;
            $payment->save();
            return redirect()->back()->with("success","Key updated successfully !");
        }
        $paymentDetail = PaymentSetting::find(1);
        return view('admin.payment_settings',['paymentDetail'=>$paymentDetail]);
    }
    
    public function paymentDetail()
    {
        $paymentDetail = PaymentDetail::get();
        return view('admin.payment_details',['paymentDetail'=>$paymentDetail]);
    }
    
    /*=================Manage Types============*/
    
    public function managetype()
    {
        $data = WorkspaceType::get();
        return view('admin.workspace_types',['data'=>$data]);
    }
    
    public function addType(Request $request)
    {
        if($request->all())
        {
            $validatedData = $request->validate([
                'name' => 'required',
                'status' => 'required',
            ]);
            if($request->id != '')
            {
                $duration = WorkspaceType::find($request->id);
            }
            else
            {
                $duration = new WorkspaceType();
            }
            $duration->name = $request->name;
            $duration->status = $request->status;
            $duration->save();
            return redirect('admin/manage_types');
        }
        $data = '';
        return view('admin.add_type',['data'=>$data]);
    }
    
    public function editType($id)
    {
        $data = WorkspaceType::find(decrypt($id));
        return view('admin.add_type',['data'=>$data]);
    }
    
    /*============Manage Durations===========*/
    
    public function manageDuration()
    {
        $data = WorkspaceDuration::get();
        return view('admin.workspace_duration',['data'=>$data]);
    }
    
    public function addDuration(Request $request)
    {
        if($request->all())
        {
            $validatedData = $request->validate([
                'name' => 'required',
                'status' => 'required',
            ]);
            if($request->id != '')
            {
                $duration = WorkspaceDuration::find($request->id);
            }
            else
            {
                $duration = new WorkspaceDuration();
            }
            $duration->name = $request->name;
            $duration->status = $request->status;
            $duration->save();
            return redirect('admin/manage_durations');
        }
        $data = '';
        return view('admin.add_duration',['data'=>$data]);
    }
    
    public function editDuration($id)
    {
        $data = WorkspaceDuration::find(decrypt($id));
        return view('admin.add_duration',['data'=>$data]);
    }
    
    /*=================Manage Features============*/
    
    public function manageFeatures()
    {
        $data = WorkspaceFeature::get();
        return view('admin.workspace_features',['data'=>$data]);
    }
    
    public function addFeature(Request $request)
    {
        if($request->all())
        {
            $validatedData = $request->validate([
                'name' => 'required',
                'status' => 'required',
            ]);
            if($request->id != '')
            {
                $feature = WorkspaceFeature::find($request->id);
            }
            else
            {
                $feature = new WorkspaceFeature();
            }
            $feature->name = $request->name;
            $feature->status = $request->status;
            $feature->save();
            return redirect('admin/manage_features');
        }
        $data = '';
        return view('admin.add_feature',["data"=>$data]);
    }
    
    public function editFeature($id)
    {
        $data = WorkspaceFeature::find(decrypt($id));
        return view('admin.add_feature',["data"=>$data]);
    }
    
    /*==============Manage Amenities============*/
    
    public function manageAmenity()
    {
        $data = WorkspaceAmenity::get();
        return view('admin.workspace_amenities',['data'=>$data]);
    }
    
    public function addAmenity(Request $request)
    {
        if($request->all())
        {
            $validatedData = $request->validate([
                'name' => 'required',
                'status' => 'required',
            ]);
            if($request->id != '')
            {
                $amenity = WorkspaceAmenity::find($request->id);
            }
            else
            {
                $amenity = new WorkspaceAmenity();
            }
            $amenity->name = $request->name;
            $amenity->status = $request->status;
            $amenity->save();
            return redirect('admin/manage_amenities');
        }
        $data = '';
        return view('admin.add_amenity',["data"=>$data]);
    }
    
    public function editAmenity($id)
    {
        $data = WorkspaceAmenity::find(decrypt($id));
        return view('admin.add_amenity',["data"=>$data]);
    }
    
    public function manageWorkspace()
    {
        $workspace = Workspace::with('workspaceType')->get();
        return view('admin.manage_workspace',["workspace"=>$workspace]);
    }
    
    public function addWorkspace(Request $request)
    {
        if($request->all())
        {
            $validatedData = $request->validate([
                'workspace_type' => 'required',
                'featured_image' => 'required',
                'workspace_name' => 'required',
                'total_seats' => 'required',
                'area' => 'required',
                'booking_price_monthly' => 'required',
                'booking_price_yearly' => 'required',
                'address' => 'required',
                'duration' => 'required',
                'about_workspace' => 'required',
                'total_rooms' => 'required',
                'total_lounge_seats' => 'required',
            ]);
            
            $workspace = new Workspace();
            $workspace->workspace_type = $request->workspace_type;
            $workspace->workspace_name = $request->workspace_name;
            $workspace->total_seats = $request->total_seats;
            $workspace->total_area = $request->area;
            $workspace->booking_price_monthly = $request->booking_price_monthly;
            $workspace->booking_price_yearly = $request->booking_price_yearly;
            $workspace->address = $request->address;
            $workspace->duration = $request->duration;
            $workspace->about_workspace = $request->about_workspace;
            $workspace->total_rooms = $request->total_rooms;
            $workspace->total_lounge_seats = $request->total_lounge_seats;
            $workspace->created_by = Auth::user()->user_type;
            if ($request->featured_image) 
            {
                $file = $request->featured_image;
                $imageType = $file->getClientmimeType();
                $fileName = $file->getClientOriginalName();
                $fileNameUnique = time() . '_' . $fileName;
                $destinationPath = public_path() . '/assets/uploads/';
                $file->move($destinationPath, $fileNameUnique);
                $workspace->featured_image = $fileNameUnique;
            }
            $workspace->save();
            return redirect('admin/manage_workspace');
        }
        $amenities = WorkspaceAmenity::get();
        $features = WorkspaceFeature::get();
        $types = WorkspaceType::get();
        $duration = WorkspaceDuration::get();
        return view('admin.add_workspace',['amenities'=>$amenities,'features'=>$features,'types'=>$types,'duration'=>$duration]);
    }
    
    /*===============Manage Faqs=============*/
    
    public function manageFaqs()
    {
        $faqs = Faq::get();
        return view('admin.manage_faq',["faqs"=>$faqs]);
    }
    public function addFaqs(Request $request)
    {
        if($request->all())
        {
            $validatedData = $request->validate([
                'question' => 'required',
                'answer' => 'required',
                'status' => 'required',
            ]);
            if($request->id != '')
            {
                $faqs = Faq::find($request->id);
            }
            else
            {
                $faqs = new Faq();
            }
            $faqs->question = $request->question;
            $faqs->answer = $request->answer;
            $faqs->status = $request->status;
            $faqs->save();
            return redirect('admin/faqs');
        }
        $faqs = '';
        return view('admin.add_faqs',['faqs'=>$faqs]);
    }
    public function editFaqs($id)
    {
        $faqs = Faq::find(decrypt($id));
        return view('admin.add_faqs',['faqs'=>$faqs]);
    }
    
    /*=============Manage Contact Us=============*/
    
    public function contactUs()
    {
        $contactUs = ContactForm::get();
        return view('admin.manage_contact_us',['contactUs'=>$contactUs]);
    }
}