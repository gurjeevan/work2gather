<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkspacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workspaces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('workspace_type')->nullable();
            $table->text('workspace_name')->nullable();
            $table->text('featured_image')->nullable();
            $table->text('total_seats')->nullable();
            $table->text('total_area')->nullable();
            $table->text('booking_price_monthly')->nullable();
            $table->text('booking_price_yearly')->nullable();
            $table->text('address')->nullable();
            $table->text('lat')->nullable();
            $table->text('long')->nullable();
            $table->text('duration')->nullable();
            $table->text('about_workspace')->nullable();
            $table->text('total_rooms')->nullable();
            $table->text('total_lounge_seats')->nullable();
            $table->text('floor_plan_images')->nullable();
            $table->text('status')->nullable();
            $table->text('created_by')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workspaces');
    }
}
