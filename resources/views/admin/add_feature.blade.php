@include('admin.includes.header')
@yield('sidebar-content')
	<div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="block">
                <div class="block-title">
                    <h2>
                        <strong>
                            @if($data != '')
                                {{'Edit Features'}}
                            @else
                                {{'Add Features'}}
                            @endif
                        </strong>
                    </h2>
                </div>
                <form action="{{url('admin/add_feature')}}" id="admin_add_city" method="post" class="form-horizontal form-bordered" autocomplete="off">
                    @csrf
					
					<input type="hidden" name="id" value="@if($data != ''){{$data->id}}@endif">
					
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control" value="@if($data != ''){{$data->name}}@else{{old('name')}}@endif" placeholder="Name">
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-3 control-label" for="Status">Status</label>
                        <div class="col-sm-9">
                            <label class="radio-inline" for="Active">
                                <input type="radio" id="Active" name="status" value="1"  @if($data != '' && $data->status == 1){{"checked='checked'"}}@else{{"checked='checked'"}}@endif> Active
                            </label>
                            <label class="radio-inline" for="Inactive">
                                <input type="radio" id="Inactive" name="status" value="0" @if($data != '' && $data->status == 0){{"checked='checked'"}}@endif> Inactive
                            </label>				
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-md btn-primary">
							    @if($data != '')
									{{'Update'}}
								@else
								    {{'Add'}}
								@endif
								
							</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@include('admin.includes.footer')