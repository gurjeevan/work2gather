@include('admin.includes.header')
@yield('sidebar-content')
	<div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="block">
                <div class="block-title">
                    <h2>
                        <strong>Manage Profile</strong>
                    </h2>
                </div>
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <form action="{{url('admin/manage_profile')}}" id="admin_manage_profile" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" autocomplete="off">
                    @csrf
					
					<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control" value="{{Auth::user()->name}}" placeholder="Name">
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">Email</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" class="form-control" value="{{Auth::user()->email}}" placeholder="Email">
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="title">Profile Image</label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control-file"  id="avatarFile">
                            <br>
                            <img id="blah" src="@if(Auth::user()->profile_image != ''){{url('/public/assets/uploads/'.Auth::user()->profile_image)}}@endif" alt="your image" height="100" width="100" @if(Auth::user()->profile_image == ''){{'style=display:none'}}@endif>
                        </div>
                    </div>
					
                    <div class="form-group form-actions">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-md btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@include('admin.includes.footer')