@include('admin.includes.header') 

@yield('sidebar-content') 

<div class="row">
<div class="col-sm-10 col-sm-offset-1">

	<!-- Horizontal Form Block -->
	<div class="block">
	
	<!-- Horizontal Form Title -->
	<div class="block-title">
		<h2><strong>Terms & Conditions</strong></h2>
	</div>
	<!-- END Horizontal Form Title -->

	<!-- Horizontal Form Content -->
	<form action="{{url('/admin/update_pages')}}" method="post" class="form-horizontal form-bordered">
	    @csrf
	    <input type="hidden" name="id" value="@if($data != '') {{encrypt($data->id)}} @endif">
	    <input type="hidden" name="slug" value="terms_condition">
	    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
			<fieldset>
			<legend>Page Title</legend>
			<div class="form-group">
				<div class="col-xs-12">
					<input name="name" class="form-control" placeholder="Page Title" value="@if($data != '') {{$data->name}} @endif">
				</div>
			</div>
		</fieldset>
		</div>
		<div class="form-group {{ $errors->has('textarea-ckeditor') ? 'has-error' : ''}}">
			<fieldset>
			<legend>Description</legend>
			<div class="form-group">
				<div class="col-xs-12">
					<textarea id="textarea-ckeditor" name="textarea_ckeditor" class="ckeditor">@if($data != '') {{$data->description}} @endif</textarea>
				</div>
			</div>
		</fieldset>
		</div>
		<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
			<label class="col-sm-3 control-label" for="Status">Status</label>
			<div class="col-sm-9">
				<label class="radio-inline" for="Active">
					<input type="radio" id="Active" name="status" value="1" @if($data != '' && $data->status == 1) {{"checked='checked'"}} @else {{"checked='checked'"}} @endif> Active
				</label>
				<label class="radio-inline" for="Inactive">
					<input type="radio" id="Inactive" value="0" name="status" @if($data != '' && $data->status == 0) {{"checked='checked'"}} @endif> Inactive
				</label>				
			</div>
		</div>
		<div class="form-group form-actions">
			<div class="col-sm-12 text-center">
				<button type="submit" class="btn btn-md btn-primary">Update</button>
			</div>
		</div>
	</form>
	<!-- END Horizontal Form Content -->
</div>
<!-- END Horizontal Form Block -->

</div>
</div>

@include('admin.includes.footer') 