@include('admin.includes.header')
@yield('sidebar-content')
	<div class="block full">
		<div class="block-title">
			<h2><strong>Manage Cities</strong></h2>
			<a href="{{url('/admin/add_city')}}">
				<button type="button" class="btn btn-primary" data-attr="add">Add City</button>
			</a>
		</div>
		<div class="table-responsive">
			<table class="table table-vcenter table-condensed table-bordered table_service">         
				<div class="sort_search">
				<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
				<div class="form-group">
					<select id="cus_sort_by" name="cus_sort_by" class="form-control">
						<option value="10">10</option>
						<option value="20">20</option>
						<option value="30">30</option>
						<option value="All">All</option>
					</select>
				</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 pull-right">
				<div class="input-group">
					<input type="text" id="search_keyword" name="search_keyword" class="form-control" placeholder="Search">
					<span class="input-group-btn">
						<button type="button" class="btn btn-primary">Search</button>
					</span>
				</div><br>
				</div>
				</div>
				<thead>
					<tr>
					<th class="text-center">S.No</th>               
					<th class="text-center">City</th>
					<th class="text-center">Status</th>
					<th class="text-center">Actions</th>
					</tr>
				</thead>		 
				<tbody>
					@if(count($cities) > 0)
						@php $i =1; @endphp
						@foreach($cities as $val)
							@php $id = $val->id; @endphp
							<tr id="row_{{$id}}">
								<td class="text-center">{{$i}}</td>
								<td class="text-center">{{ucfirst($val->city)}}</td>
								<td class="text-center">
									@if($val->status == 1)
										<a href="javascript:void(0)" class="updateStatus" data-status="0" data-id="{{$id}}" data-target="manage_cities" onClick="return confirm('Are you sure you want to deactivate this city?');">
											<button class="btn btn-danger">{{'Active'}}</button>
										</a>
									@else
										<a href="javascript:void(0)" class="updateStatus" data-status="1" data-id="{{$id}}" data-target="manage_cities" onClick="return confirm('Are you sure you want to activate this city?');">
											<button class="btn btn-danger">{{'Deactive'}}</button>
										</a>
									@endif
								</td>
								<td class="text-center">
									<div class="btn-group">
										<a data-toggle="tooltip" href="{{url('/admin/edit_city/'.encrypt($val->id))}}" title="Edit" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i></a>
										<a href="javascript:void(0)" onClick="return confirm('Are you sure you want to delete this city?');" data-id="{{$id}}" data-target="manage_cities" data-toggle="tooltip" title="Delete" class="btn btn-sm btn-danger deleteData"><i class="fa fa-times"></i></a>
									</div>
								</td>	
							</tr>
						@php $i++; @endphp
						@endforeach
					@endif
				</tbody>		 
			</table>
		</div>
	</div>
<!-- Modal -->
<div class="modal fade" id="cityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cityTitle">Add City</h5>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn_title">Add</button>
      </div>
    </div>
  </div>
</div>
@include('admin.includes.footer')
