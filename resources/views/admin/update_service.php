<?php include 'includes/header.php'; ?> 
<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="block">
            <div class="block-title">
                <h2><strong>Update Customer Service</strong></h2>
            </div>
            <form action="#" method="post" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="title">Service Title</label>
                    <div class="col-sm-9">
                        <input type="text" id="title" name="title" class="form-control" value="What was the need for a regulatory law for the real estate sector?">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="des">Description</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" id="des" name="des" rows="4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</textarea>
                    </div>
                </div>
				<div class="form-group">
			        <label class="col-sm-3 control-label" for="Status">Status</label>
					<div class="col-sm-9">
						<label class="radio-inline" for="Active">
							<input type="radio" id="Active" name="Status" checked> Active
						</label>
						<label class="radio-inline" for="Inactive">
							<input type="radio" id="Inactive" name="Status"> Inactive
						</label>				
					</div>
		        </div>
                <div class="form-group form-actions">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-md btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include 'includes/footer.php'; ?>