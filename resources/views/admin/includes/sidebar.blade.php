
<div id="sidebar">
  <div id="sidebar-scroll">
    <div class="sidebar-content">
      <a href="{{url('/admin')}}" class="sidebar-brand">
		    <span class="sidebar-nav-mini-hide"><strong>Work2Gather</strong></span>
      </a>
      <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
        <div class="sidebar-user-avatar">
            <a href="{{url('/admin')}}">
			    <img src="{{url('/public/assets/uploads/'.Auth::user()->profile_image)}}" alt="userimg">
            </a>
        </div>
        <div class="sidebar-user-name">@if(Auth::user()) {{ ucfirst(Auth::user()->name) }} @endif</div>
      </div>
      <ul class="sidebar-nav">
        <li>
          <a href="index.php" class="active"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
        </li>
        <!--<li>-->
        <!--  <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-lock sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Admin Management</span></a>-->
        <!--  <ul>-->
        <!--    <li>-->
        <!--      <a href="add-admin.php">Add Admin</a>-->
        <!--    </li>-->
        <!--    <li>-->
        <!--      <a href="admin-list.php">Manage Admin</a>-->
        <!--    </li>-->
        <!--  </ul>-->
        <!--</li>-->
		    <!--<li>-->
      <!--    <a href="manage_project.php"><i class="fa fa-building sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage Project</span></a>-->
      <!--  </li>-->
      <!--  <li>-->
      <!--    <a href="#" class="sidebar-nav-menu">-->
      <!--      <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>-->
      <!--      <i class="fa fa-user-plus sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Banner</span>-->
      <!--    </a>-->
      <!--    <ul>-->
      <!--      <li>-->
      <!--        <a href="add-banner.php">Add Banner</a>-->
      <!--      </li>-->
      <!--      <li>-->
      <!--        <a href="banner-list.php">Manage Banner</a>-->
      <!--      </li>-->
      <!--    </ul>-->
      <!--  </li>-->
		<li>
          <a href="#" class="sidebar-nav-menu">
            <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
            <i class="fa fa-server sidebar-nav-icon"></i>
            <span class="sidebar-nav-mini-hide">Manage Users</span>
          </a>
          <ul>
            <li>
              <a href="{{url('/admin/add_users')}}">Add Users</a>
            </li>
            <li>
              <a href="{{url('/admin/manage_users')}}">Manage Users</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="#" class="sidebar-nav-menu">
            <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
            <i class="fa fa-server sidebar-nav-icon"></i>
            <span class="sidebar-nav-mini-hide">Manage Workspace</span>
          </a>
          <ul>
            <li>
              <a href="{{url('/admin/manage_workspace')}}">Manage Workspace</a>
            </li>
            <li>
              <a href="{{url('/admin/manage_amenities')}}">Amenities</a>
            </li>
            <li>
              <a href="{{url('/admin/manage_features')}}">Features</a>
            </li>
            <li>
              <a href="{{url('/admin/manage_durations')}}">Duration</a>
            </li>
            <li>
              <a href="{{url('/admin/manage_types')}}">Workspace Types</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="#" class="sidebar-nav-menu">
            <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
            <i class="fa fa-life-ring sidebar-nav-icon"></i>
            <span class="sidebar-nav-mini-hide">Manage Webpages</span>
          </a>
          <ul>
            <li>
              <a href="{{url('/admin/about_us')}}">About Us</a>
            </li>
            <!--<li>-->
            <!--  <a href="Contact-us.php">Contact Us</a>-->
            <!--</li>-->
            <li>
              <a href="{{url('/admin/privacy_policy')}}">Privacy Policy</a>
            </li>
            <li>
              <a href="{{url('/admin/terms_condition')}}">Terms & Condition</a>
            </li>
            <li>
              <a href="{{url('/admin/faqs')}}">Faqs</a>
            </li>
            <li>
              <a href="{{url('/admin/contact_us')}}">Contact Us</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="#" class="sidebar-nav-menu">
            <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
            <i class="fa fa-server sidebar-nav-icon"></i>
            <span class="sidebar-nav-mini-hide">Manage Payments</span>
          </a>
          <ul>
            <li>
              <a href="{{url('/admin/payment_settings')}}">Payment Settings</a>
            </li>
             <li>
              <a href="{{url('/admin/manage_payments')}}">Payment Details</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="#" class="sidebar-nav-menu">
            <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
            <i class="fa fa-server sidebar-nav-icon"></i>
            <span class="sidebar-nav-mini-hide">Theme Settings</span>
          </a>
          <ul>
            <li>
              <a href="{{url('/admin/manage_cities')}}">Manage Cities</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="#" class="sidebar-nav-menu">
            <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
            <i class="fa fa-server sidebar-nav-icon"></i>
            <span class="sidebar-nav-mini-hide">Settings</span>
          </a>
          <ul>
            <li>
              <a href="{{url('/admin/manage_profile')}}">Manage Profile</a>
            </li>
             <li>
              <a href="{{url('/admin/change_password')}}">Change Password</a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</div>
