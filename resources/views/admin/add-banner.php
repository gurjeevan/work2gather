<?php include 'includes/header.php'; ?> 
<div class="row">
<div class="col-sm-8 col-sm-offset-2">
	<div class="block">
		<div class="block-title">
		<h2><strong>Add Banner</strong></h2>
	</div>
	<form action="javascript:void();" method="post" class="form-horizontal form-bordered">
		<div class="form-group">
			<label class="col-sm-3 control-label" for="banner_title">Banner Title</label>
			<div class="col-sm-9">
				<input type="text" id="banner_title" name="banner_title" class="form-control" placeholder="Banner Title">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="banner_img">Banner Image</label>
			<div class="col-sm-9">
				<input type="File" id="banner_img" name="banner_img" class="form-control">				
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="Status">Status</label>
			<div class="col-sm-9">
				<label class="radio-inline" for="Active">
					<input type="radio" id="Active" name="Status"> Active
				</label>
				<label class="radio-inline" for="Inactive">
					<input type="radio" id="Inactive" name="Status"> Inactive
				</label>				
			</div>
		</div>
		<div class="form-group form-actions">
			<div class="col-sm-12 text-center">
				<button type="submit" class="btn btn-md btn-primary">Add Banner</button>
			</div>
		</div>
	</form>
</div>
</div>
</div>
<?php include 'includes/footer.php'; ?>