@include('admin.includes.header')
@yield('sidebar-content')
	<div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="block">
                <div class="block-title">
                    <h2>
                        <strong>
                            @if($city == '')
                                {{'Add City'}}
                            @else
                                {{'Edit City'}}
                            @endif
                        </strong>
                    </h2>
                </div>
                <form action="{{url('admin/add_city')}}" id="admin_add_city" method="post" class="form-horizontal form-bordered">
                    @csrf
					
					<input type="hidden" name="id" value="@if($city != '') {{encrypt($city->id)}} @endif">
					
                    <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">City Name</label>
                        <div class="col-sm-9">
                            <input type="text" id="city" name="city" class="form-control" value="@if($city != ''){{$city->city}}@else{{old('city')}}@endif" placeholder="City Name">
                            {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-3 control-label" for="Status">Status</label>
                        <div class="col-sm-9">
                            <label class="radio-inline" for="Active">
                                <input type="radio" id="Active" name="status" value="1"  @if($city != '' && $city->status == 1) {{ "checked='checked'"}} @else {{"checked='checked'"}} @endif> Active
                            </label>
                            <label class="radio-inline" for="Inactive">
                                <input type="radio" id="Inactive" value="0" name="status" @if($city != '' && $city->status == 0) {{ "checked='checked'"}} @endif> Inactive
                            </label>				
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-md btn-primary">
								@if($city == '')
									{{'Add'}}
								@else
									{{'Update'}}
								@endif
							</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@include('admin.includes.footer')