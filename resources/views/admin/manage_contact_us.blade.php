@include('admin.includes.header')
@yield('sidebar-content')
	<div class="block full">
		<div class="block-title">
			<h2><strong>Contact Us</strong></h2>
			
		</div>
		<div class="table-responsive">
			<table class="table table-vcenter table-condensed table-bordered table_service">         
				<div class="sort_search">
				<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
				<div class="form-group">
					<select id="cus_sort_by" name="cus_sort_by" class="form-control">
						<option value="10">10</option>
						<option value="20">20</option>
						<option value="30">30</option>
						<option value="All">All</option>
					</select>
				</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 pull-right">
				<div class="input-group">
					<input type="text" id="search_keyword" name="search_keyword" class="form-control" placeholder="Search">
					<span class="input-group-btn">
						<button type="button" class="btn btn-primary">Search</button>
					</span>
				</div><br>
				</div>
				</div>
				<thead>
					<tr>
    					<th class="text-center">S.No</th>               
    					<th class="text-center">Name</th>               
    					<th class="text-center">Email</th>
    				    <th class="text-center">Phone Number</th>
    					<th class="text-center">Message</th>
					</tr>
				</thead>		 
				<tbody>
					@if(count($contactUs) > 0)
						@php $i =1; @endphp
						@foreach($contactUs as $val)
							@php $id = $val->id; @endphp
							<tr id="row_{{$id}}">
								<td class="text-center">{{$i}}</td>
								<td class="text-center">{{ucfirst($val->name)}}</td>
								<td class="text-center">{{$val->email}}</td>
								<td class="text-center">{{$val->phone_number}}</td>
								<td class="text-center">{{$val->message}}</td>
							</tr>
						@php $i++; @endphp
						@endforeach
					@endif
				</tbody>		 
			</table>
		</div>
	</div>
@include('admin.includes.footer')