<?php include 'includes/header.php'; ?> 
<div class="row">
<div class="col-sm-8 col-sm-offset-2">
	<div class="block">
		<div class="block-title">
		<h2><strong>Update Banner</strong></h2>
	</div>
	<form action="javascript:void();" method="post" class="form-horizontal form-bordered">
		<div class="form-group">
			<label class="col-sm-3 control-label" for="banner_title">Banner Title</label>
			<div class="col-sm-9">
				<input type="text" id="banner_title" name="banner_title" class="form-control" Value="Banner Title">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="banner_img">Banner Image</label>
			<div class="col-sm-9">
				<input style="width:60%;float:left;" type="File" id="banner_img" name="banner_img" class="form-control">
				<img style="margin-left:10px;margin-top:-7px;" src="img/placeholders/photos/photo1.jpg" width="50" height="50">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="Status">Status</label>
			<div class="col-sm-9">
				<label class="radio-inline" for="Active">
					<input type="radio" id="Active" name="Status"> Active
				</label>
				<label class="radio-inline" for="Inactive">
					<input type="radio" id="Inactive" name="Status"> Inactive
				</label>				
			</div>
		</div>
		<div class="form-group form-actions">
			<div class="col-sm-12 text-center">
				<button type="submit" class="btn btn-md btn-primary">Update Banner</button>
			</div>
		</div>
	</form>
</div>
</div>
</div>
<?php include 'includes/footer.php'; ?>