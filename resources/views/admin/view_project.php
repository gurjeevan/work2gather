<?php include 'includes/header.php'; ?> 

<!-- Datatables Content -->
<div class="col-sm-8 col-sm-offset-2">
<div class="block full">
   <div class="block-title">
      <h2><strong>View Project Details</strong></h2>
   </div>
   <div class="table-responsive">
      <table class="table table-vcenter table-condensed table_detail">         		
        <tbody>
            <tr>
               <th class="text-right" style="width:30%;">Project Image</th>
               <td class="text-left"><img src="img/placeholders/photos/photo1.jpg" alt="imgage" width="50" height="50"></td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Project Title</th>
               <td class="text-left">2 BHK Apartment</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Agent Type</th>
               <td class="text-left">Dealer/Owner/Builder</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Agent Name</th>
               <td class="text-left">M. Abid</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Project For</th>
               <td class="text-left">Rent/Sell</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Project Type</th>
               <td class="text-left">Residential/Commertial/Plot</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Construction Status</th>
               <td class="text-left">New Lounch/Under Construction/Ready To Move</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Project Name</th>
               <td class="text-left">Independence House/Independence Villa</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Max Price</th>
               <td class="text-left">$2,285,500</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Min Price</th>
               <td class="text-left">$2,285,500</td>              	
            </tr>											
			<tr>
               <th class="text-right" style="width:30%;">SQ.FT</th>
               <td class="text-left">1,250</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">State</th>
               <td class="text-left">Sindh</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">City</th>
               <td class="text-left">Rawalpindi</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Locality</th>
               <td class="text-left">Lahore</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Full Address</th>
               <td class="text-left">Sector B, Bahria Town, Lahore</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Bed Rooms</th>
               <td class="text-left">2</td>              	
            </tr>
			<tr>
               <th class="text-right" style="width:30%;">Bath Rooms</th>
               <td class="text-left">3</td>              	
            </tr>		
        </tbody>		 
      </table>
   </div>
</div>
</div>
<div class="clearfix"></div>
<!-- END Datatables Content -->

<?php include 'includes/footer.php'; ?>