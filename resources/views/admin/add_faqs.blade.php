@include('admin.includes.header') 

@yield('sidebar-content')

<div class="row">
<div class="col-sm-10 col-sm-offset-1">

	<!-- Horizontal Form Block -->
	<div class="block">
	
	<!-- Horizontal Form Title -->
	<div class="block-title">
		<h2>
		    <strong>
		        @if($faqs != '')
			        {{"Edit Faqs"}}
			    @else
			        {{"Add Faqs"}}
			    @endif
			</strong>
		</h2>
	</div>
	<!-- END Horizontal Form Title -->

	<!-- Horizontal Form Content -->
	<form action="{{url('/admin/add_faqs')}}" method="post" class="form-horizontal form-bordered">
	    @csrf
	    <input type="hidden" name="id" value="@if($faqs != ''){{$faqs->id}}@endif">
	    <div class="form-group {{ $errors->has('question') ? 'has-error' : ''}}">
			<fieldset>
			<legend>Question</legend>
			<div class="form-group">
				<div class="col-xs-12">
					<input name="question" class="form-control" placeholder="Question" value="@if($faqs != ''){{$faqs->question}}@else{{old('question')}}@endif">
				</div>
			</div>
		</fieldset>
		</div>
		<div class="form-group {{ $errors->has('answer') ? 'has-error' : ''}}">
			<fieldset>
			<legend>Answer</legend>
			<div class="form-group">
				<div class="col-xs-12">
					<textarea id="textarea-ckeditor" name="answer" class="ckeditor">@if($faqs != ''){{$faqs->answer}}@else{{old('answer')}}@endif</textarea>
				</div>
			</div>
		</fieldset>
		</div>
		<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
			<label class="col-sm-3 control-label" for="Status">Status</label>
			<div class="col-sm-9">
				<label class="radio-inline" for="Active">
					<input type="radio" id="Active" name="status" value="1" {{"checked='checked'"}}> Active
				</label>
				<label class="radio-inline" for="Inactive">
					<input type="radio" id="Inactive" value="0" name="status"> Inactive
				</label>					
			</div>
		</div>
		<div class="form-group form-actions">
			<div class="col-sm-12 text-center">
				<button type="submit" class="btn btn-md btn-primary">
				    @if($faqs != '')
				        {{"Update"}}
				    @else
				        {{"Add"}}
				    @endif
				</button>
			</div>
		</div>
	</form>
	<!-- END Horizontal Form Content -->
</div>
<!-- END Horizontal Form Block -->

</div>
</div>

@include('admin.includes.footer')