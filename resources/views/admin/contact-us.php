<?php include 'includes/header.php'; ?> 

<div class="row">
<div class="col-sm-8 col-sm-offset-2">

	<!-- Horizontal Form Block -->
	<div class="block">
	
	<!-- Horizontal Form Title -->
	<div class="block-title">
		<h2><strong>Contact Us</strong></h2>
	</div>
	<!-- END Horizontal Form Title -->

	<!-- Horizontal Form Content -->
	<form action="#" method="post" class="form-horizontal form-bordered">
		<div class="form-group">
			<label class="col-sm-3 control-label" for="number">Mobile Number</label>
			<div class="col-sm-9">
				<input type="text" maxlength="10" id="number" name="number" class="form-control" placeholder="Mobile Number">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="Email">Email</label>
			<div class="col-sm-9">
				<input type="email" id="Email" name="Email" class="form-control" placeholder="Enter Email..">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="address">Address</label>
			<div class="col-sm-9">
				<textarea id="address" name="address" rows="2" class="form-control" placeholder="Full Address"></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="description">Description</label>
			<div class="col-sm-9">
				<textarea id="description" name="description" rows="2" class="form-control" placeholder="Description"></textarea>
			</div>
		</div>
		<div class="form-group form-actions">
			<div class="col-sm-12 text-center">
				<button type="submit" class="btn btn-md btn-primary">Submit</button>
			</div>
		</div>
	</form>
	<!-- END Horizontal Form Content -->
</div>
<!-- END Horizontal Form Block -->

</div>
</div>

<?php include 'includes/footer.php'; ?>