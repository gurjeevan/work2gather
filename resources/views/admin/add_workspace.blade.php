<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Droppable - Default functionality</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
  #draggable { width: 100px; height: 100px; padding: 0.5em; float: left; margin: 10px 10px 10px 0; }
  #droppable { width: 150px; height: 150px; padding: 0.5em; float: left; margin: 10px; }
 
  </style>
  <style type="text/css">
body
{
    font-family: Arial;
    font-size: 10pt;
}
img
{
    height: 100px;
    width: 100px;
    margin: 2px;
}
.draggable
{
    filter: alpha(opacity=60);
    opacity: 0.6;
}
.dropped
{
    position: static !important;
}
#dvSource, #dvDest
{
    border: 5px solid #ccc;
    padding: 5px;
    min-height: 100px;
    width: 430px;
}
</style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#draggable" ).draggable({
       revert: "invalid",
        refreshPositions: true,
        drag: function (event, ui) {
            ui.helper.addClass("draggable");
        },
    });
    $( "#droppable" ).droppable({
       
      drop: function( event, ui ) {
        ui.draggable.addClass("dropped");
        //$("#droppable").append(ui.draggable);
      }
      stop: function (event, ui) {
            ui.helper.removeClass("draggable");
            
        }
     
    });
  } );

  
  </script>
  <script type="text/javascript">
$(function () {
    $("#dvSource img").draggable({
        revert: "invalid",
        refreshPositions: true,
        drag: function (event, ui) {
            ui.helper.addClass("draggable");
        },
        stop: function (event, ui) {
            ui.helper.removeClass("draggable");
            var image = this.src.split("/")[this.src.split("/").length - 1];
            if ($.ui.ddmanager.drop(ui.helper.data("draggable"), event)) {
                alert(image + " dropped.");
            }
            else {
                alert(image + " not dropped.");
            }
        }
    });
    $("#dvDest").droppable({
        drop: function (event, ui) {
            if ($("#dvDest img").length == 0) {
                $("#dvDest").html("");
            }
            ui.draggable.addClass("dropped");
            $("#dvDest").append(ui.draggable);
        }
    });
});
</script>
</head>
<body>
    
<div id="dvSource">
    <img alt="" src="images/Chrysanthemum.jpg" />
    <img alt="" src="images/Desert.jpg" />
    <img alt="" src="images/Hydrangeas.jpg" />
    <img alt="" src="images/Jellyfish.jpg" />
    <img alt="" src="images/Koala.jpg" />
    <img alt="" src="images/Lighthouse.jpg" />
    <img alt="" src="images/Penguins.jpg" />
    <img alt="" src="images/Tulips.jpg" />
</div>
<hr />
<div id="dvDest">
    Drop here
</div>
  
 
<div id="draggable" class="ui-widget-content">
  <p>Drag me to my target</p>
</div>
 <div id="divmain">
<div id="droppable" class="ui-widget-header">

</div>
</div>
 
 
</body>
</html>

<!--@include('admin.includes.header')-->
<!--@yield('sidebar-content')-->
<!--<style>-->
<!--    #dragThis {-->
<!--    width: 6em;-->
<!--    height: 6em;-->
<!--    padding: 0.5em;-->
<!--    border: 3px solid #ccc;-->
<!--    border-radius: 0 1em 1em 1em;-->
<!--}-->
<!--</style>-->
<!-- <style>-->
<!--  #draggable { width: 100px; height: 100px; padding: 0.5em; float: left; margin: 10px 10px 10px 0; }-->
<!--  #droppable { width: 150px; height: 150px; padding: 0.5em; float: left; margin: 10px; }-->
<!--  </style>-->
<!--	<div class="row">-->
<!--        <div class="col-sm-8 col-sm-offset-2">-->
<!--            <div class="block">-->
<!--                <div class="block-title">-->
<!--                    <h2>-->
<!--                        <strong>-->
<!--                           {{'Add Workspace'}}-->
<!--                        </strong>-->
<!--                    </h2>-->
<!--                </div>-->
<!--                <div id="dragThis">-->
<!--                    <ul>-->
<!--                        <li id="posX"></li>-->
<!--                        <li id="posY"></li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div id="draggable" class="ui-widget-content">-->
<!--                  <p>Drag me to my target</p>-->
<!--                </div>-->
                 
<!--                <div id="droppable" class="ui-widget-header">-->
<!--                  <p>Drop here</p>-->
<!--                </div>-->
<!--                <form action="{{url('admin/add_workspace')}}" id="admin_add_workspace" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" autocomplete="off">-->
<!--                    @csrf-->
					
<!--					<input type="hidden" name="id" value="">-->
					
<!--                    <div class="form-group {{ $errors->has('workspace_type') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Type of Workspace</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <select class="form-control chzn-select" name="workspace_type">-->
<!--                                <option value="">Type of Workspace</option>-->
<!--                                @if(count($types) > 0)-->
<!--                                    @foreach($types as $typ)-->
<!--                                        <option value="{{$typ->id}}">{{ucfirst($typ->name)}}</option>-->
<!--                                    @endforeach-->
<!--                                @endif-->
<!--                            </select>-->
<!--                            {!! $errors->first('workspace_type', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('featured_image') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Featured Image</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="file" name="featured_image" class="form-control">-->
<!--                            {!! $errors->first('featured_image', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('workspace_name') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Workspace Name</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="text" name="workspace_name" class="form-control" placeholder="Workspace Name" value="{{old('workspace_name')}}">-->
<!--                            {!! $errors->first('workspace_name', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
                    
<!--                    <div class="form-group {{ $errors->has('total_seats') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Total Seats</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="text" name="total_seats" class="form-control" placeholder="Total Seats" value="{{old('total_seats')}}">-->
<!--                            {!! $errors->first('total_seats', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('area') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Area in sq. Feet</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="text" name="area" class="form-control" placeholder="Area in sq. Feet" value="{{old('area')}}">-->
<!--                            {!! $errors->first('area', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('booking_price_monthly') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Booking Price(Monthly)</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="text" name="booking_price_monthly" class="form-control" placeholder="Booking Price(Monthly)" value="{{old('booking_price_monthly')}}">-->
<!--                            {!! $errors->first('booking_price_monthly', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('booking_price_yearly') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Booking Price(Yearly)</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="text" name="booking_price_yearly" class="form-control" placeholder="Booking Price(Yearly)" value="{{old('booking_price_yearly')}}">-->
<!--                            {!! $errors->first('booking_price_yearly', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Address</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="text" name="address" class="form-control" placeholder="Address" value="{{old('address')}}">-->
<!--                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('duration') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Select Duration</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <select class="form-control chzn-select" name="duration">-->
<!--                                <option value="">Select Duration</option>-->
<!--                                @if(count($duration) > 0)-->
<!--                                    @foreach($duration as $dur)-->
<!--                                        <option value="{{$dur->id}}">{{ucfirst($dur->name)}}</option>-->
<!--                                    @endforeach-->
<!--                                @endif-->
<!--                            </select>-->
<!--                            {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('features') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Select Features</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <select class="form-control chzn-select" name="features[]" multiple>-->
<!--                                @if(count($features) > 0)-->
<!--                                    @foreach($features as $fea)-->
<!--                                        <option value="{{$fea->id}}">{{ucfirst($fea->name)}}</option>-->
<!--                                    @endforeach-->
<!--                                @endif-->
<!--                            </select>-->
<!--                            {!! $errors->first('features', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('amenities') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Select Amenities</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <select class="form-control chzn-select" name="amenities[]" multiple>-->
<!--                                @if(count($amenities) > 0)-->
<!--                                    @foreach($amenities as $am)-->
<!--                                        <option value="{{$am->id}}">{{ucfirst($am->name)}}</option>-->
<!--                                    @endforeach-->
<!--                                @endif-->
<!--                            </select>-->
<!--                            {!! $errors->first('amenities', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('about_workspace') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">About Workspace</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <textarea name="about_workspace" class="form-control" placeholder="About Workspace" value="{{old('about_workspace')}}"></textarea>-->
<!--                            {!! $errors->first('about_workspace', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('total_rooms') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Total Rooms</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="number" name="total_rooms" class="form-control" placeholder="Number of Rooms" value="{{old('total_rooms')}}">-->
<!--                            {!! $errors->first('total_rooms', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('total_lounge_seats') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Total Lounge Seats</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="number" name="total_lounge_seats" class="form-control" placeholder="Number of Lounge Seats" value="{{old('total_lounge_seats')}}">-->
<!--                            {!! $errors->first('total_lounge_seats', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
                    
<!--                    <div class="form-group {{ $errors->has('floor_plans') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Floor Plans(Images)</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="file" name="floor_plans[]" class="form-control">-->
<!--                            {!! $errors->first('floor_plans', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('images') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Images</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="file" name="images[]" class="form-control" id="gallery-photo-add" multiple>-->
<!--                            {!! $errors->first('images', '<p class="help-block">:message</p>') !!}-->
<!--                            <div class="gallery"></div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group {{ $errors->has('images') ? 'has-error' : ''}}">-->
<!--                        <label class="col-sm-3 control-label" for="title">Videos</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <input type="file" name="videos[]" class="form-control">-->
<!--                            {!! $errors->first('videos', '<p class="help-block">:message</p>') !!}-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label class="col-sm-3 control-label" for="Status">Status</label>-->
<!--                        <div class="col-sm-9">-->
<!--                            <label class="radio-inline" for="Active">-->
<!--                                <input type="radio" id="Active" name="status" value="1" {{"checked='checked'"}}> Active-->
<!--                            </label>-->
<!--                            <label class="radio-inline" for="Inactive">-->
<!--                                <input type="radio" id="Inactive" value="0" name="status"> Inactive-->
<!--                            </label>				-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="form-group form-actions">-->
<!--                        <div class="col-sm-12 text-center">-->
<!--                            <button type="submit" class="btn btn-md btn-primary">-->
<!--                                {{'Add'}}-->
<!--                            </button>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--@include('admin.includes.footer')-->
<!--<script>-->
<!--    $('#dragThis').draggable(-->
<!--    {-->
<!--        drag: function(){-->
<!--            var offset = $(this).offset();-->
<!--            var xPos = offset.left;-->
<!--            var yPos = offset.top;-->
<!--            $('#posX').text('x: ' + xPos);-->
<!--            $('#posY').text('y: ' + yPos);-->
<!--        }-->
<!--    });-->
<!--</script>-->
<!--<script>-->
<!--  $( function() {-->
<!--    $( "#draggable" ).draggable();-->
<!--    $( "#droppable" ).droppable({-->
<!--      drop: function( event, ui ) {-->
<!--        $( this )-->
<!--          .addClass( "ui-state-highlight" )-->
<!--          .find( "p" )-->
<!--            .html( "Dropped!" );-->
<!--      }-->
<!--    });-->
<!--  } );-->
<!--  </script>-->