@include('admin.includes.header')
@yield('sidebar-content')
	<div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="block">
                <div class="block-title">
                    <h2>
                        <strong>Change Password</strong>
                    </h2>
                </div>
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <form action="{{url('admin/change_password')}}" id="admin_change_password" method="post" class="form-horizontal form-bordered">
                    @csrf
					
					<input type="hidden" name="id" value="">
					
                    <div class="form-group {{ $errors->has('current_password') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">Current Password</label>
                        <div class="col-sm-9">
                            <input type="password" id="current_pwd" name="current_password" class="form-control" value="{{old('current_password')}}" placeholder="Current Password">
                            {!! $errors->first('current_password', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    
                    <div class="form-group {{ $errors->has('new_password') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">New Password</label>
                        <div class="col-sm-9">
                            <input type="password" id="new_pwd" name="new_password" class="form-control" value="{{old('new_password')}}" placeholder="New Password">
                            {!! $errors->first('new_password', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="title">Confirm Password</label>
                        <div class="col-sm-9">
                            <input type="password" id="confirm_pwd" name="new_password_confirmation" class="form-control" value="{{old('new_password_confirmation')}}" placeholder="Confirm Password">
                        </div>
                    </div>
					
                    <div class="form-group form-actions">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-md btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@include('admin.includes.footer')