<?php include 'includes/header.php'; ?> 
<!-- Datatables Content -->
<div class="block full">  
    <div class="block-title">
      <h2><strong>Manage Projects</strong></h2>
    </div>   
   <div class="table-responsive">
      <table class="table table-vcenter table-condensed table-bordered table_design">         
		<div class="sort_search">
		 <div class="col-sm-2">
		 <div class="form-group">
			<select id="cus_sort_by" name="cus_sort_by" class="form-control">
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option value="All">All</option>
			</select>
		 </div>
		 </div>
		 <div class="col-sm-3 pull-right">
		 <div class="input-group">
			<input type="text" id="search_keyword" name="search_keyword" class="form-control" placeholder="Search">
			<span class="input-group-btn">
				<button type="button" class="btn btn-primary">Search</button>
			</span>
		 </div><br>
		 </div>
		 </div>
		<thead>
            <tr>
               <th class="text-center">S.No</th>               
               <th class="text-center">Project Image</th>
               <th class="text-center">Project Name</th>
               <th class="text-center">Project Type</th>
               <th class="text-center">Price</th>
               <th class="text-center">Location</th>
               <th class="text-center">Actions</th>
            </tr>
        </thead>		 
        <tbody>
            <tr>
               <td class="text-center">1</td>
               <td class="text-center"><img src="img/placeholders/photos/photo1.jpg" alt="imgage" width="50" height="50"></td>
               <td class="text-center">2 BHK Apartment</td>
               <td class="text-center">Residential</td>
               <td class="text-center">$2,285,500</td>
               <td class="text-center">Lahore</td>
			   <td class="text-center">
				<div class="btn-group">
					<a href="view_project.php" data-toggle="tooltip" title="View Detail" class="btn btn-sm btn-danger"><i class="fa fa-eye"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Hidden Detail" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
				</div>
			    </td>	
            </tr>
			<tr>
               <td class="text-center">2</td>
               <td class="text-center"><img src="img/placeholders/photos/photo1.jpg" alt="imgage" width="50" height="50"></td>
               <td class="text-center">2 BHK Apartment</td>
               <td class="text-center">Commertial</td>
               <td class="text-center">$2,285,500</td>
               <td class="text-center">Lahore</td>
			   <td class="text-center">
				<div class="btn-group">
					<a href="view_project.php" data-toggle="tooltip" title="View Detail" class="btn btn-sm btn-danger"><i class="fa fa-eye"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Hidden Detail" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
				</div>
			   </td>	
            </tr>
			<tr>
               <td class="text-center">3</td>
               <td class="text-center"><img src="img/placeholders/photos/photo1.jpg" alt="imgage" width="50" height="50"></td>
               <td class="text-center">2 BHK Apartment</td>
               <td class="text-center">Residential</td>
               <td class="text-center">$2,285,500</td>
               <td class="text-center">Lahore</td>
			   <td class="text-center">
				<div class="btn-group">
					<a href="view_project.php" data-toggle="tooltip" title="View Detail" class="btn btn-sm btn-danger"><i class="fa fa-eye"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Hidden Detail" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
				</div>
			    </td>	
            </tr>
			<tr>
               <td class="text-center">4</td>
               <td class="text-center"><img src="img/placeholders/photos/photo1.jpg" alt="imgage" width="50" height="50"></td>
               <td class="text-center">2 BHK Apartment</td>
               <td class="text-center">Plot</td>
               <td class="text-center">$2,285,500</td>
               <td class="text-center">Lahore</td>
			   <td class="text-center">
				<div class="btn-group">
					<a href="view_project.php" data-toggle="tooltip" title="View Detail" class="btn btn-sm btn-danger"><i class="fa fa-eye"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Hidden Detail" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
				</div>
			    </td>	
            </tr>
        </tbody>		 
      </table>
   </div>
</div>
<!-- END Datatables Content -->
<?php include 'includes/footer.php'; ?>