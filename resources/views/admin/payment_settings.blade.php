@include('admin.includes.header')
@yield('sidebar-content')
	<div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="block">
                <div class="block-title">
                    <h2>
                        <strong>Payment Settings</strong>
                    </h2>
                </div>
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <form action="{{url('admin/payment_settings')}}" id="admin_payment_settings" method="post" class="form-horizontal form-bordered" autocomplete="off">
                    @csrf
					<input type="hidden" name="id" value="@if($paymentDetail != ''){{$paymentDetail->id}}@endif">
					<div class="form-group {{ $errors->has('api_key') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">Api Key</label>
                        <div class="col-sm-9">
                            <input type="text" name="api_key" class="form-control" value="@if($paymentDetail != ''){{$paymentDetail->api_key}}@else{{old('api_key')}}@endif" placeholder="Api Key">
                            {!! $errors->first('api_key', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    
                    <div class="form-group {{ $errors->has('secret_key') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">Secret Key</label>
                        <div class="col-sm-9">
                            <input type="text" name="secret_key" class="form-control" value="@if($paymentDetail != ''){{$paymentDetail->secret_key}}@else{{old('secret_key')}}@endif" placeholder="Secret Key">
                            {!! $errors->first('secret_key', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    
                    <div class="form-group form-actions">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-md btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@include('admin.includes.footer')